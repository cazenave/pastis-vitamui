package fr.gouv.vitamui.pastis.model;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import fr.gouv.vitamui.pastis.util.RNGConstants;

public class JsonFromRng {

	 String name;

	    String type;

	    String cardinality;

	    String valuleOrData;

	    String dataType;

	    String value;

	    String documentation;

	    int level;

	    Long id;

	    Long parentId;

	    @JsonIgnore
	    ElementProperties parent;

	    List<ElementProperties> children = new ArrayList<ElementProperties>();


	    public List<ElementProperties> getChildren() {
	        return this.children;
	    }

	    public void setChildren(List<ElementProperties> children) {
	        this.children = children;
	    }

	    public String getName() {
	        return this.name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public String getType() {
	        return this.type;
	    }

	    public void setType(String type) {
	        this.type = type;
	    }

	    public String getValuleOrData() {
	        return this.valuleOrData;
	    }

	    public void setValuleOrData(String dataType) {
	        this.valuleOrData = dataType;
	    }

	    public String getCardinality() {
	        return this.cardinality;
	    }

	    public void setCardinality(String cardinality) {
	        if(null != RNGConstants.CardinalityMap.get(cardinality)) {
	            this.cardinality = RNGConstants.CardinalityMap.get(cardinality);
	        }else {
	            this.cardinality = cardinality;
	        }
	    }

	    public String getValue() {
	        return this.value;
	    }

	    public void setValue(String value) {
	        this.value = value;
	    }

	    public int getLevel() {
	        return this.level;
	    }

	    public void setLevel(int level) {
	        this.level = level;
	    }

	    public String getDataType() {
	        return this.dataType;
	    }

	    public void setDataType(String dataType) {
	        this.dataType = dataType;
	    }


	    public String getDocumentation() {
	       return this.documentation;
	    }

	    public void setDocumentation(String documentation) {
	        this.documentation = documentation;
	    }

	    public Long getId() {
	        return this.id;
	    }

	    public void setId(Long id) {
	        this.id = id;
	    }

	    public Long getParentId() {
	        return this.parentId;
	    }

	    public void setParentId(Long parentId) {
	        this.parentId = parentId;
	    }

	    //@JsonIgnore
	    public ElementProperties getParent() {
	        return this.parent;
	    }

	    //@JsonIgnore
	    public void setParent(ElementProperties parent) {
	        this.parent = parent;
	    }
}
