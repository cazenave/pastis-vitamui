package fr.gouv.vitamui.pastis.model;

public class OntologyDSL {

	public OntologyDSL() {
	}

	public String contrat_acces;
	public String dsl_request;
	public String query;
	public String and;
	public String eq;
	public String filter;
	public int limit;
	public String projection;
	public String tenant_id;


	public String getContrat_acces() {
		return contrat_acces;
	}
	public void setContrat_acces(String contrat_acces) {
		this.contrat_acces = contrat_acces;
	}
	public String getDsl_request() {
		return dsl_request;
	}
	public void setDsl_request(String dsl_request) {
		this.dsl_request = dsl_request;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getAnd() {
		return and;
	}
	public void setAnd(String and) {
		this.and = and;
	}
	public String getEq() {
		return eq;
	}
	public void setEq(String eq) {
		this.eq = eq;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getProjection() {
		return projection;
	}
	public void setProjection(String projection) {
		this.projection = projection;
	}
	public String getTenant_id() {
		return tenant_id;
	}
	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String toString() {
        return "{\r\n" +
        		"  \"contrat_acces\": \"CINES-1\",\r\n" +
        		"  \"dsl_request\": {\r\n" +
        		"		\"$query\": {\r\n" +
        		"			\"$and\": [\r\n" +
        		"				{\r\n" +
        		"					\"$eq\": {\r\n" +
        		"						\"SedaField\": \"DocumentType +\"\r\n" +
        		"					}\r\n" +
        		"				}\r\n" +
        		"			]\r\n" +
        		"		},\r\n" +
        		"		\"$filter\": {\r\n" +
        		"			\"$limit\": 1\r\n" +
        		"		},\r\n" +
        		"		\"$projection\": {}\r\n" +
        		"  },\r\n" +
        		"  \"tenant_id\": \"1\"\r\n" +
        		"}";
	}
}
