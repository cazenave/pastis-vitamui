/*
 * Copyright CINES, 2016 Alexandre Granier Ce
 * logiciel est un programme informatique servant à créer une interface Web pour valider des formats
 * de fichiers. Ce logiciel est régi par la licence CeCILL-C soumise au droit français et respectant
 * les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou
 * redistribuer ce programme sous les conditions de la licence CeCILL-C telle que diffusée par le
 * CEA, le CNRS et l'INRIA sur le site "http://www.cecill.info". En contrepartie de l'accessibilité
 * au code source et des droits de copie, de modification et de redistribution accordés par cette
 * licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule
 * une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits
 * patrimoniaux et les concédants successifs. A cet égard l'attention de l'utilisateur est attirée
 * sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement
 * et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre,
 * qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des
 * professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs
 * sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des
 * conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus
 * généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. Le fait que vous
 * puissiez accéder à cet en-tête signifie que vous avez pris connaissance de la licence CeCILL-C,
 * et que vous en avez accepté les termes.
 */
package fr.gouv.vitamui.pastis;

import fr.gouv.vitamui.commons.api.logger.VitamUILogger;
import fr.gouv.vitamui.commons.api.logger.VitamUILoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.core.env.Environment;

import javax.ws.rs.core.Application;


/**
 *
 * @author RACHID Sala <rachid@cines.fr>
 *
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class PastisRestApplication extends Application {


    public PastisRestApplication(){}

    @Autowired
    private Environment env;

    private static final VitamUILogger LOGGER = VitamUILoggerFactory.getInstance(PastisRestApplication.class);

    public static void main(final String[] args) {
        SpringApplication app = new SpringApplicationBuilder(PastisRestApplication.class).properties("spring.config.name:ui-pastis-application").build();
        app.run(args);
    }


    public void run(final String... args) throws Exception {
        LOGGER.info("VITAMUI SpringBoot Application started:");
        LOGGER.info("spring.config.name: " + env.getProperty("spring.config.name"));
        LOGGER.info("spring.application.name: " + env.getProperty("spring.application.name"));
    }

}
