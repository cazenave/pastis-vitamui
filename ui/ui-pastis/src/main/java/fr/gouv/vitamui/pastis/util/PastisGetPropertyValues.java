package fr.gouv.vitamui.pastis.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PastisGetPropertyValues {

	public Properties getPropValues()  {

		InputStream otherpath = getClass().getClassLoader().getResourceAsStream("config.properties");
		Properties pastisProperties = new Properties();

		try {
			pastisProperties.load(otherpath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pastisProperties;
	}

	public Properties getTestPropValues()  {
		String rootPath = Thread.currentThread().getContextClassLoader().getResource("../classes/").getPath();
		String pastisConfigFile = rootPath + "config.properties";

		Properties pastisProperties = new Properties();

		try {
			pastisProperties.load(new FileInputStream(pastisConfigFile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pastisProperties;
	}



}
