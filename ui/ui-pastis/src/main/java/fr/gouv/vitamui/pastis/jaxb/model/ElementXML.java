/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.jaxb.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
@XmlRootElement (name="rng:element")
public class ElementXML extends BaliseXML{

    ValueXML valueXML;


    @XmlElement (name="rng:value")
    public ValueXML getValueXML() {
        return valueXML;
    }

    public void setValueXML(ValueXML valueXML) {
        this.valueXML = valueXML;
    }
}
