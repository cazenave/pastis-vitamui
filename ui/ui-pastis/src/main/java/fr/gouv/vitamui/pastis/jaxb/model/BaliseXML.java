/*
 * Copyright CINES Centre Informatique National de l'Enseignement Supérieur, 2017
 * Tout droit d'utilisation strictement soumis à l'approbation du CINES
 */
package fr.gouv.vitamui.pastis.jaxb.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import fr.gouv.vitamui.pastis.model.ElementProperties;
import fr.gouv.vitamui.pastis.util.RNGConstants.Cardinality;
import fr.gouv.vitamui.pastis.util.RNGConstants.GroupOrChoice;

/**
 * @author rachid Sala <rachid@cines.fr>
 */
@XmlRootElement
public class BaliseXML{

	public BaliseXML() {
		super();
	}

	String name;

	String dataType;

	String cardinality;

	String groupOrChoice;

	public static ElementProperties elementStatic = new ElementProperties();

	public static BaliseXML baliseXMLStatic;

	public static ElementProperties elementStaticRoot = new ElementProperties();

	List<BaliseXML> children = new ArrayList<BaliseXML>();



	@XmlAttribute
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlAttribute (name="type")
	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	BaliseXML parent;

	//transient pour ne pas avoir une boucle infini pere/children
	@XmlTransient
	public BaliseXML getParent() {
		return parent;
	}

	public void setParent(BaliseXML parent) {
		this.parent = parent;
	}

	// XmlAnyElement pour etre le plus generique
	@XmlAnyElement
	public List<BaliseXML> getChildren() {
		return children;
	}

	public void setChildren(List<BaliseXML> children) {
		this.children = children;
	}

	public String getCardinality() {
		return cardinality;
	}

	public String setCardinality(Cardinality cardinality) {
		return this.cardinality = cardinality.getLabel();
	}

	public String getGroupOrChoice() {
		return groupOrChoice;
	}

	public void setGroupOrChoice(GroupOrChoice groupOrChoice) {
		this.groupOrChoice = groupOrChoice.getLabel();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.name;
	}

	/**
	 *
	 * @param node node représentant l'arbre ElementProperties correspondant au json reçu du front
	 * @param profondeur profondeur du noeud utile pour le front angular
	 * @param parentNode noeud parent utilisé dans la récursivité pour lié parent & children
	 * @return
	 */
	public static BaliseXML buildBaliseXMLTree( ElementProperties node, int profondeur, BaliseXML parentNode ){

		ValueXML valueRNG = null;
		DataXML dataRNG = null;
		BaliseXML cardinalityRNG = null;
		BaliseXML elementOrAttributeRNG = null;
		AnnotationXML annotationXML = null;
		DocumentationXML documentationXML = null;
		BaliseXML groupOrChoice = null;


		// si node a une value on crée une balise value
		if(null != node.getValue() && !node.getValue().equals("undefined")) {
			valueRNG = new ValueXML();
			valueRNG.setValue(node.getValue());

		}

		//si node a une getValuleOrData=data on crée une balise data
		if(null != node.getValueOrData() && !node.getValueOrData().equals("undefined")) {
			if(node.getValueOrData().equals("data")) {
				dataRNG = new DataXML();
			}
		}

		//on set le type de donnée, pour celui créé que ce soit value ou data
		if(null != node.getDataType() && !node.getDataType().equals("undefined")) {
			//if(null != valueRNG) {
				//valueRNG.setDataType(node.getDataType());
			//}else
				//if(null != dataRNG) {
				dataRNG.setDataType(node.getDataType());
			//}
		}

		if(null != node.getDocumentation() ) {
			annotationXML = new AnnotationXML();
			documentationXML = new DocumentationXML();
			documentationXML.setDocumentation(node.getDocumentation());
			annotationXML.setDocumentationXML(documentationXML);
		}

		if(null != node.getType() && !node.getType().equals("undefined")) {
			if(node.getType().equals("element")) {
				elementOrAttributeRNG = new ElementXML();
			} else if(node.getType().equals("attribute")) {
				elementOrAttributeRNG = new AttributeXML();
			}
			if(null != node.getName() && !node.getName().equals("undefined")) {
				elementOrAttributeRNG.setName(node.getName());
			}
		}

		if(null != documentationXML) {
			elementOrAttributeRNG.getChildren().add(annotationXML);
			annotationXML.setParent(elementOrAttributeRNG);
		}

		// Check node's and its children's cardinality
		if(node.getCardinality() != null ) {
			if(node.getCardinality().equals(Cardinality.zeroOrMore.getLabel())) {
				cardinalityRNG = new ZeroOrMoreXML();
				if(elementOrAttributeRNG != null) {
					elementOrAttributeRNG.setCardinality(Cardinality.zeroOrMore);
				}
			} else if(node.getCardinality().equals(Cardinality.oneOrMore.getLabel())) {
				cardinalityRNG = new OneOrMoreXML();
				if(elementOrAttributeRNG != null) {
					elementOrAttributeRNG.setCardinality(Cardinality.oneOrMore);
				}
			} else if(node.getCardinality().equals(Cardinality.optional.getLabel())) {
				cardinalityRNG = new OptionalXML();
				if(elementOrAttributeRNG != null) {
					elementOrAttributeRNG.setCardinality(Cardinality.optional);
				}
			}
		}

//		if(node.getGroupOrChoice() != null ) {
//			if(node.getGroupOrChoice().equals(GroupOrChoice.group.getLabel())) {
//				groupOrChoice = new GroupXML();
//				if(elementOrAttributeRNG != null) {
//					elementOrAttributeRNG.setGroupOrChoice(GroupOrChoice.group);
//				}
//			} else if(node.getGroupOrChoice().equals(GroupOrChoice.choice.getLabel())) {
//				groupOrChoice = new ChoiceXml();
//				if(elementOrAttributeRNG != null) {
//					elementOrAttributeRNG.setGroupOrChoice(GroupOrChoice.choice);
//				}
//			}
//		}

		BaliseXML currentXmlTag = null;

		// 1. Check if it is an element
		if(null != elementOrAttributeRNG) {

			System.out.println(elementOrAttributeRNG.getName());
			// 1.1 Check if the element has cardinality
			if( null != cardinalityRNG ) {
				cardinalityRNG.getChildren().add(elementOrAttributeRNG);
				elementOrAttributeRNG.setParent(cardinalityRNG);
				currentXmlTag = cardinalityRNG;
			}
//
//			if( null != groupOrChoice ) {
//				groupOrChoice.getChildren().add(elementOrAttributeRNG);
//				elementOrAttributeRNG.setParent(groupOrChoice);
//				currentXmlTag = groupOrChoice;
//			}

			else {
				currentXmlTag = elementOrAttributeRNG;
				//1.2. Check if it's the first grammarnode (Archive transfer)
				if(parentNode == null) {
					GrammarXML grammar = new GrammarXML();
					StartXML start = new StartXML();
					start.setParent(grammar);
					//start.setChildren(currentXmlTag.getChildren());
					grammar.getChildren().add(start);
					currentXmlTag = grammar;

					//elementOrAttributeRNG = start;
					System.out.println("Is the root node");
				}

//				if (parentNode != null && !parentNode.getChildren().isEmpty()
//						&& parentNode.getName() !=null
//						&& !parentNode.getName().equals("ArchiveTransfer")) {
//					BaliseXML parentTagIsCardinality = parentNode.getChildren().stream()
//							.filter(element -> element instanceof OptionalXML
//									|| element instanceof ZeroOrMoreXML
//									|| element instanceof OneOrMoreXML)
//							.findFirst()
//							.orElse(null);
//					if (parentTagIsCardinality != null
//					cardinalityRNG		&& !parentNode.getName().equals(node.getParent().getName())) {
//						parentTagIsCardinality.getChildren().add(elementOrAttributeRNG);
//						currentXmlTag.setParent(parentTagIsCardinality);
//					}
//				}
			}

			// 2. Check data tag
            if(null != dataRNG) {
            	if (!currentXmlTag.getChildren().isEmpty() &&
            			(currentXmlTag.getChildren().get(0) instanceof ElementXML
            					||currentXmlTag.getChildren().get(0) instanceof AttributeXML)){
                	currentXmlTag.getChildren().get(0).getChildren().add(dataRNG);
                    dataRNG.setParent(currentXmlTag);
                }else {
                	currentXmlTag.getChildren().add(dataRNG);
                    dataRNG.setParent(currentXmlTag);
                }

				//cardinalityRNG.setParent(currentXmlTag.getChildren().get(0));

				// 3. Check value tag
			}  if(null != valueRNG) {
				// If Children is empty
				if(currentXmlTag.getChildren().isEmpty() ) {
					if (currentXmlTag instanceof ElementXML) {
						((ElementXML)currentXmlTag).setValueXML(valueRNG);
						valueRNG.setParent(currentXmlTag);

					} else if (currentXmlTag instanceof AttributeXML) {
						((AttributeXML)currentXmlTag).setValueXML(valueRNG);
						valueRNG.setParent(currentXmlTag);
						//ccx

					}
					// If children is Element or Attribute, set  accordingly
				} else {
					if (currentXmlTag instanceof ElementXML) {
						((ElementXML)currentXmlTag).setValueXML(valueRNG);
						valueRNG.setParent(currentXmlTag);

					} else if (currentXmlTag  instanceof AttributeXML) {
						((AttributeXML)currentXmlTag).setValueXML(valueRNG);
						valueRNG.setParent(currentXmlTag);

					}
				}
			}
		}

		if(null != currentXmlTag) {

			if(null != parentNode) {
				BaliseXML optionalWithChidren = parentNode.getChildren()
						.stream().filter(cardinality-> cardinality instanceof OptionalXML
								|| cardinality instanceof ZeroOrMoreXML
								|| cardinality instanceof OneOrMoreXML )
						.findAny()
						.orElse(null);

				Boolean optionalHasAlreadyCurrentTag = optionalWithChidren == null
						? false : optionalWithChidren.children.contains(currentXmlTag);

				if (!optionalHasAlreadyCurrentTag) {
					currentXmlTag.setParent(parentNode);
					parentNode.getChildren().add(currentXmlTag);
				}


			}else {
				baliseXMLStatic = currentXmlTag;
			}
		}

		if (currentXmlTag instanceof GrammarXML) {
			buildBaliseXMLTree( node, profondeur + 1, currentXmlTag.getChildren().get(0));
		}
		else {
			for( ElementProperties next : node.getChildren() ) {
				if (currentXmlTag instanceof OptionalXML || currentXmlTag instanceof OneOrMoreXML
					|| currentXmlTag instanceof ZeroOrMoreXML)  {
					buildBaliseXMLTree( next, profondeur + 1, currentXmlTag.getChildren().get(0));
				} else {
					buildBaliseXMLTree( next, profondeur + 1, currentXmlTag );
				}
			}
		}

		return currentXmlTag;
	}
}
