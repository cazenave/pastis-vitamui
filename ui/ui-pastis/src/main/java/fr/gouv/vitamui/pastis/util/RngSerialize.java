//package fr.gouv.vitamui.pastis.util;
//
//import java.io.IOException;
//import java.util.List;
//
//import com.fasterxml.jackson.core.JsonGenerationException;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonStreamContext;
//import com.fasterxml.jackson.databind.SerializerProvider;
//import com.fasterxml.jackson.databind.ser.std.StdSerializer;
//
//import fr.cines.pastis.rng.model.Attribute;
//import fr.cines.pastis.rng.model.Element;
//import fr.cines.pastis.rng.model.Grammar;
//import fr.cines.pastis.rng.model.NsName;
//import fr.cines.pastis.rng.model.Start;
//
//public class RngSerialize extends StdSerializer<Grammar> {
//
//	private Element rootElement;
//	private List<Element> children;
//	private Start startElement;
//	private RNGConstants rngConstants;
//	private JsonParser jsonParser;
//
//
//
//	public RngSerialize(Class<Grammar> t) {
//	        super(t);
//	    }
//
//		@Override
//		public void serialize(Grammar grammar, JsonGenerator jsonGen, SerializerProvider provider)
//				throws IOException, JsonGenerationException {
//			startElement = grammar.getStart();
//			rootElement = startElement.getElement();
//			buildJsonTree(rootElement, jsonGen);
//
//
//		}
//
//		public void buildJsonTree(Element currentRootElement, JsonGenerator jGen) throws JsonGenerationException, IOException {
//
//			jGen.writeStartObject();
//
//			JsonStreamContext ctx = jGen.getOutputContext();
//			JsonStreamContext parsingContext = ctx;
//	        Object parent = parsingContext.getParent();
//
//
//
//			System.out.println("Element type : " + currentRootElement.getClass().getSimpleName());
//
//
//			//Set cardinality for the current node
//
//			// Considering that anyname has ony an execept inside
//			if (currentRootElement.getAnyName() != null ) {
//				System.out.println("The Cardinality of : " + currentRootElement.getAnyName().getClass().getSimpleName() +
//						" is " + currentRootElement.getAnyName().getClass().getSimpleName());
//				for(Element nsname : currentRootElement.getAnyName().getExcept().getNsName().getElement()) {
//					jGen.writeStringField("name", currentRootElement.getName());
//					jGen.writeStringField("type", "to be set");
//					jGen.writeStringField("cardinality", this.findCardinality(currentRootElement));
//					jGen.writeStringField("valueOrData", "nsName");
//					jGen.writeNumberField("level", 0);
//					jGen.writeNumberField("id", 5);
//					jGen.writeNumberField("parentId", 0);
//				}
//
//					buildJsonTree(currentRootElement.getAnyName(), jGen);
//			}
//			//
//			if (currentRootElement.getExcept()!= null) {
//				jGen.writeStringField("cardinality", "0-1");
//				jGen.writeStringField("valueOrData", "to be set");
//				jGen.writeNumberField("level", 0);
//				jGen.writeNumberField("id", 5);
//				jGen.writeNumberField("parentId", 0);
//				buildJsonTree(currentRootElement.getExcept(), jGen);
//
//			}
//
//			if (currentRootElement.getAttribute() != null) {
//				System.out.println("The Cardinality of : " + currentRootElement.getName() +
//						" is " + currentRootElement.getClass().getSimpleName());
//				jGen.writeStringField("cardinality", "0-1");
//				jGen.writeStringField("valueOrData", "to be set");
//				jGen.writeNumberField("level", 0);
//				jGen.writeNumberField("id", 5);
//				jGen.writeNumberField("parentId", 0);
//				if (currentRootElement.getAttribute().getData() != null) {
//					buildJsonTree(currentRootElement.getAttribute().getData(), jGen);
//				}
//				if (currentRootElement.getAttribute().getAnyName()!= null) {
//					buildJsonTree(currentRootElement.getAttribute().getAnyName(), jGen);
//				}
//
//			}
//			//Treat a zero or more
//			if (currentRootElement.getZeroOrMore() != null) {
//				if (currentRootElement.getZeroOrMore().getAttribute() != null  ){
//					buildJsonTree(currentRootElement.getZeroOrMore(), jGen);
//				}
//				if (currentRootElement.getZeroOrMore().getElement() != null) {
//					for (Element element : currentRootElement.getElement()) {
//							buildJsonTree(element, jGen);
//					}
//				}
//			}
//
//			//Treat a one or more
//			if (currentRootElement.getOneOrMore() != null) {
//				if (currentRootElement.getOneOrMore().getAttribute() != null ){
//					buildJsonTree(currentRootElement.getOneOrMore(), jGen);
//				}
//				if (currentRootElement.getOneOrMore().getElement() != null) {
//					for (Element element : currentRootElement.getElement()) {
//							buildJsonTree(element, jGen);
//					}
//				}
//			}
//
//
//			//Treat an optional
//			if (currentRootElement.getOptional() != null) {
//				if (currentRootElement.getOptional().getAttribute() != null ){
//					buildJsonTree(currentRootElement.getOptional(), jGen);
//				}
//				if (currentRootElement.getOptional().getElement() != null) {
//					for (Element element : currentRootElement.getElement()) {
//							buildJsonTree(element, jGen);
//					}
//				}
//			}
//
//			if (currentRootElement.getNsName() != null) {
//				//for ( element : currentRootElement.getNsName()) {
//				//	buildJsonTree(element, jGen);
//				//}
//				jGen.writeStringField("valueOrData", "to be set");
//				buildJsonTree(currentRootElement.getZeroOrMore().getAttribute(),jGen);
//			}
//			jGen.writeEndObject();
//			jGen.writeEndArray();
//
//
//			System.out.println("Current root : " + currentRootElement.getName());
//
//			// For the elements on the same root
//			for(Element element : currentRootElement.getElement()) {
//				if(element.getElement() != null) {
//					//jsonGen.writeArrayFieldStart(currentElement.getOneOrMore().toString().getClass().getName());
//				} else {
//					jGen.writeStringField("name", element.getName());
//
//				}
//				buildJsonTree(element, jGen);
//
//			}
//
//			jGen.writeEndObject();
//
//		}
//
//
//
//		public String getAttribute(Attribute att,JsonGenerator jgen) {
//
//			return "test";
//
//		}
//
//		public String findCardinality(Element currentRootElement) {
//			String cardinality = "null";
//			if (currentRootElement.getOneOrMore()!= null) {
//				System.out.println("The Cardinality of : " + currentRootElement.getName() +
//						" is " + currentRootElement.getOneOrMore().getClass().getSimpleName());
//				cardinality = currentRootElement.getOneOrMore().getClass().getSimpleName();
//
//			}
//
//			if (currentRootElement.getZeroOrMore() != null) {
//				System.out.println("The Cardinality of : " + currentRootElement.getName() +
//						" is " + currentRootElement.getZeroOrMore().getClass().getSimpleName());
//				cardinality = currentRootElement.getZeroOrMore().getClass().getSimpleName();
//
//			}
//
//			if (currentRootElement.getOptional() != null) {
//				System.out.println("The Cardinality of : " + currentRootElement.getName() +
//						" is " + currentRootElement.getOptional().getClass().getSimpleName());
//				cardinality = currentRootElement.getOptional().getClass().getSimpleName();
//			}
//
//			return cardinality;
//		}
//
//}
