import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToggleSidenavService {

    constructor() { }
    isOpened = new BehaviorSubject<boolean>(false);

    show() {
        this.isOpened.next(true);
        console.log("SideNav is opened")
    }
    hide() {
        this.isOpened.next(false);
        console.log("SideNav is closed")
    }
}
