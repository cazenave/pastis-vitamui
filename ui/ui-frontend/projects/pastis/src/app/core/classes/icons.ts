// Button names should reflect an action
export enum IconsEnum {
    'pastis-close-sidenav' = 'close-sidenav',
    'pastis-back-crete-profile' = 'arrow-back',
    'pastis-save' = 'save',
    'pastis-setting' = 'setting',
}