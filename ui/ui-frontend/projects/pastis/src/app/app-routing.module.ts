import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AccountComponent, ActiveTenantGuard, AppGuard, AuthGuard,
} from 'ui-frontend-common';
import { AppComponent } from './app.component';


const routes: Routes = [
  {
    // we use PORTAL_APP as our appId so that the AppGuard won't find a profile with this appId
    // and we'll be redirected to the Portal Application
    path: 'portal',
    component: AppComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'PORTAL_APP' }
  },
  {
    path: 'account',
    component: AccountComponent,
    canActivate: [AuthGuard, AppGuard],
    data: { appId: 'ACCOUNTS_APP' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    ActiveTenantGuard,
    AuthGuard
  ]
})
export class AppRoutingModule { }
