import { registerLocaleData } from '@angular/common';
import { default as localeFr } from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VitamUICommonModule,BASE_URL, ENVIRONMENT, WINDOW_LOCATION, LoggerModule } from 'ui-frontend-common';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { CoreModule } from './core/core.module';
import { UserActionsModule } from './user-actions/shared/user-actions.module';
import { SharedModule } from './shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ProfileComponent } from './profile/profile.component';
import { CreateProfileComponent } from './profile/create-profile/create-profile.component';
import { ListProfileComponent } from './profile/list-profile/list-profile.component';
import { EditProfileComponent } from './profile/edit-profile/edit-profile.component';
import { FileTreeModule } from './file-tree/file-tree.module';
import { UserActionsComponent } from './user-actions/user-actions.component';
import { FileTreeComponent } from './file-tree/file-tree.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserActionsComponent,
    FileTreeComponent,
    ProfileComponent, CreateProfileComponent, ListProfileComponent, EditProfileComponent
      ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    VitamUICommonModule,
    AppRoutingModule,
      HttpClientModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatProgressSpinnerModule,
      MatGridListModule,
      MatToolbarModule,
      MatSidenavModule,
      MatTabsModule,
      UserActionsModule,
      CoreModule,
      FileTreeModule,
      SharedModule,
      LoggerModule.forRoot()
  ],
  providers: [
    Title,
    LoggerModule,
    { provide: BASE_URL, useValue: '/portal-api' },
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: ENVIRONMENT, useValue: environment },
    { provide: WINDOW_LOCATION, useValue: window.location },
    // { provide: ErrorHandler, useClass: GlobalErrorHandler },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
