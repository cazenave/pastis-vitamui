import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';



import { CoreModule } from '../core/core.module';
import { FileTreeModule } from '../file-tree/file-tree.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { UserActionsModule } from '../user-actions/shared/user-actions.module';
import { SharedModule } from '../shared/shared.module';

// Services

import { AppComponent } from '../app.component';
import { UserActionsComponent } from '../user-actions/user-actions.component';
import { FileTreeComponent } from '../file-tree/file-tree.component';


// To put into another module - separate from app module!!



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ProfileComponent } from '../profile/profile.component';
import { CreateProfileComponent } from '../profile/create-profile/create-profile.component';
import { ListProfileComponent } from '../profile/list-profile/list-profile.component';
import { EditProfileComponent } from '../profile/edit-profile/edit-profile.component';





// Move to a proper module since it doesnt have its own module
//import { UserActionUploadComponent } from './user-actions/user-action-upload/user-action-upload.component';


/*In order to be able to use two-way data binding for form inputs you need to import
the FormsModule package in your Angular module. */
@NgModule({
  declarations: [
    AppComponent,
    UserActionsComponent,
    FileTreeComponent,
    ProfileComponent, CreateProfileComponent, ListProfileComponent, EditProfileComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTabsModule,
    UserActionsModule,
    CoreModule,
    FileTreeModule,
    SharedModule
  ],
  exports:[
      HttpClientModule,
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      BrowserAnimationsModule,
      MatProgressSpinnerModule,
      MatGridListModule,
      MatToolbarModule,
      MatSidenavModule,
      MatTabsModule,
      UserActionsModule,
      CoreModule,
      FileTreeModule,
      SharedModule
  ],
  bootstrap: [AppComponent],

})
export class HomeModule { }
