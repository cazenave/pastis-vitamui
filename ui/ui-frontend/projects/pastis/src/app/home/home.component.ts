import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, ChangeDetectionStrategy, SimpleChanges, } from '@angular/core';
import { FileNode } from '../file-tree/classes/file-node';
//import { CdkTextareaAutosize } from '@angular/cdk/text-field/typings/autosize';
import { PastisSpinnerService } from '../shared/pastis-spinner/pastis-spinner-service';
import { Meta } from '@angular/platform-browser';
import { timer } from 'rxjs';
import { ToggleSidenavService } from '../core/services/toggle-sidenav.service';
import { RegisterIconsService } from '../core/services/register-icons.service';


export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
  outlinecolor: string;
  icon: string;
  class: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

  @ViewChild('treeSelector', { static: true }) tree: any;

 // @ViewChild('autosize', { static: false }) autosize: CdkTextareaAutosize;

  nodeToSend:FileNode;


  loading = false;

  events: string[] = [];
  opened: boolean;

  shouldRun = true;

  tiles: Tile[] = [
    { text: 'Profil d\'archivage', cols: 2, rows: 2, color: 'lightgreen', outlinecolor: 'grey', icon: 'edit',class:'pastis-slide-nav-profile'},
    { text: 'Options', cols: 10, rows: 1, color: 'orange', outlinecolor: 'grey', icon: 'apps', class:'pastis-header-with-sidebav' },
    { text: 'Metadonnes', cols: 10, rows: 1, color: 'lightblue', outlinecolor: 'grey', icon: 'star',class:'pastis-center-content' },
  ];

  constructor(private spinnerService: PastisSpinnerService,private meta: Meta,
    private sideNavService : ToggleSidenavService, private iconService:RegisterIconsService) {
    timer(5000).subscribe(() => {
      this.meta.removeTag('name="viewport"');
      this.meta.addTag({ name: 'viewport', content: 'width=device-width,height=device-height, initial-scale=1, shrink-to-fit=yes' })
      console.log("Viewport :", this.meta.getTag('name=viewport').content);
    })
    this.sideNavService.isOpened.subscribe(status=>{
      this.opened = status;
    })

  }

  ngOnInit() {
    this.iconService.registerIcons();
  }

  openSideNav(){
    this.opened = true;
  }


  closeSideNav(){
    this.opened = false;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.spinnerService.isLoading.subscribe(status => {
        this.loading = status;
        console.log("Status from behavior : ", status)
      });
    });
    console.log("Afte check : ", this.loading)

  }

}
