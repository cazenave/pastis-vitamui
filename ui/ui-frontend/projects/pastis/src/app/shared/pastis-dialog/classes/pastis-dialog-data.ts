import { SedaData } from "../../../file-tree/classes/seda-data";
import { FileNode } from "../../../file-tree/classes/file-node";

export interface PastisDialogData {
    titleDialog: string;
    subTitleDialog: string;
    fileNode: FileNode;
    sedaNode: SedaData;
  }