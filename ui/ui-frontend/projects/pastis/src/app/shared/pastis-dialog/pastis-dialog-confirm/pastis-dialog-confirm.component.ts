import { Component, OnInit, Inject } from '@angular/core';
import { SedaData } from '../../../file-tree/classes/seda-data';
import { PastisDialogData } from '../classes/pastis-dialog-data';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PastisDialogComponent } from '../pastis-dialog.component';
import { SedaService } from '../../../core/services/seda.service';

@Component({
  selector: 'app-pastis-dialog-confirm',
  templateUrl: './pastis-dialog-confirm.component.html',
  styleUrls: ['./pastis-dialog-confirm.component.scss']
})
export class PastisDialogConfirmComponent implements OnInit {


  clickedYes:string;

  constructor(
    public dialogConfirmRef: MatDialogRef<PastisDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogReceivedData: PastisDialogData,
    public sedaService: SedaService) {
  }


  ngOnInit() {
    this.clickedYes = "yes";
    console.log("Data received on confirm dialog : %o", this.dialogReceivedData);

  }

  onNoClick(): void {
    console.log("Clicked no");
    this.dialogConfirmRef.close();
  }

  onYesClick(): void {
    //this.dialogReveivedData.nodeName = this.selectedSedaElement[0];
    console.log("Clicked ok on dialog : %o", this.dialogReceivedData);
    
  }

  getToolTipData(data) {
    if (data && data.length) {
      return data.nodeName
    }
  }
  ngAfterViewInit(): void {
    //this.sedaListElements.focus()
  }
  ngOnDestroy(): void {

  }


}
