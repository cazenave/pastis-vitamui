export const environment = {
  production: true,
  apiPastisUrl: 'https://dev.vitamui.com:8585',
  apiOntologyUrl: 'http://10.100.129.51:8080',
  name: 'prod'
};
