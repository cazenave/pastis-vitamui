#!/usr/bin/env bash


     # Start Iam Security
     cd ../api/api-security/security-internal 
     mvn spring-boot:run -Dspring-boot.run.noverify

     cd /tools

     # Start Iam Server Internal
     cd ../api/api-iam/iam-internal
     mvn spring-boot:run -Dspring-boot.run.noverify

     cd /tools

     # Start Iam Server External
     cd ../api/api-iam/iam-external
     mvn spring-boot:run -Dspring-boot.run.noverify

     cd /tools

     # Start Cas Server
     cd ../cas/cas-server
     java -Dspring.config.location=src/main/config/cas-server-application-dev.yml -jar target/cas-server.war

     cd /tools

     cd ../ui/ui-identity
     mkdir -p $1/target/src/main
     rm -rf $1/target/src/main/config
     cp -r $1/src/main/config $1/target/src/main/config     
     mvn spring-boot:run -Dspring-boot.run.noverify


     cd /tools

     # Start UI Portal back
     cd ../ui/ui-portal
     mkdir -p $1/target/src/main
     rm -rf $1/target/src/main/config
     cp -r $1/src/main/config $1/target/src/main/config     
     mvn spring-boot:run -Dspring-boot.run.noverify

     cd /tools

     # Start UI Identity front
     cd ../ui/ui-frontend
     npm install
     npm run start:identity

     cd /tools

     # Start UI Portal front
     cd ../ui/ui-frontend
     npm install
     npm run start:portal



