#!/usr/bin/env bash
#set -x

     # Start UI Portal back
     cd ../ui/ui-pastis
     nohup mvn spring-boot:run -Dspring-boot.run.noverify &


     # Start UI Identity front
     cd ../ui-frontend
     npm install
     nohup npm run start:identity &
     nohup npm run start:portal &
     nohup npm run start:pastis &
