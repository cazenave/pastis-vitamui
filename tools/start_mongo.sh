#!/usr/bin/env bash
echo =================================
echo
(
     flock -e -n 200

     echo =========== Starting MONGO ==========
     pushd docker/mongo ; ./start_dev.sh ; popd
) 200>/tmp/external.lockfile

echo =================================
