#!/bin/bash


docker-compose -f Mongofile.yml up -d

sleep 2

docker exec -it vitamui-mongo /bin/bash -c "mongo --port=27018 < /scripts/000_replicaset_dev-create.js;sleep 5;mongo --port=27018 < /scripts/00_check_replicaset-create.js"

docker exec -it vitamui-mongo bash -c "cat /scripts/*-insert.js |  mongo --port=27018 "

echo "vitamui-mongo is started"


docker-compose up
