use cas;

print("START cas_services_ref.js");

db.services.insert({
	"_id": NumberInt(1),
	"_class": "org.apereo.cas.services.RegexRegisteredService",
	"serviceId": "^https://.*.vitamui.com:9000.*",
	"name": "Portal Application",
	"logoutType" : "FRONT_CHANNEL",
	"logoutUrl": "https://dev.vitamui.com:9000/logout",
	"attributeReleasePolicy": {
  		"_class": "org.apereo.cas.services.ReturnAllAttributeReleasePolicy"
	}
});

db.services.insert({
	"_id" : NumberInt(2),
	"_class": "org.apereo.cas.services.RegexRegisteredService",
	"serviceId": "^https://.*.vitamui.com:9001.*",
	"name": "Identity Access Management Application",
	"logoutType" : "FRONT_CHANNEL",
	"logoutUrl": "https://dev.vitamui.com:9001/logout",
	"attributeReleasePolicy": {
  		"_class": "org.apereo.cas.services.ReturnAllAttributeReleasePolicy"
	}
});

db.services.insert({
	"_id" : NumberInt(3),
	"_class": "org.apereo.cas.services.RegexRegisteredService",
	"serviceId": "^https://.*.vitamui.com:9001.*",
	"name": "Identity Admin Access Management Application",
	"logoutType" : "FRONT_CHANNEL",
	"logoutUrl": "https://dev.vitamui.com:9001/logout",
	"attributeReleasePolicy": {
  		"_class": "org.apereo.cas.services.ReturnAllAttributeReleasePolicy"
	}
});

print("END cas_services_ref.js");
