use security;

if (! db.getUser("mongod_dbuser_security")) {
    db.createUser(
         {
          user: "mongod_dbuser_security",
          pwd: "mongod_dbpwd_security",
          roles: [{"db": "security", "role": "readWrite"}]
        }
    );
}
use  iam;

if (! db.getUser("mongod_dbuser_iam")) {
    db.createUser(
         {
          user: "mongod_dbuser_iam",
          pwd: "mongod_dbpwd_iam",
          roles: [{"db": "iam", "role": "readWrite"}]
        }
    );
}
use  cas;

if (! db.getUser("mongod_dbuser_cas")) {
    db.createUser(
         {
          user: "mongod_dbuser_cas",
          pwd: "mongod_dbpwd_cas",
          roles: [{"db": "cas", "role": "readWrite"}]
        }
    );
}
use  admin;

if (! db.getUser("mongod_dbuser_admin")) {
    db.createUser(
         {
          user: "mongod_dbuser_admin",
          pwd: "mongod_dbpwd_admin",
          roles: [{"db": "admin", "role": "userAdminAnyDatabase"}, {"db": "admin", "role": "backup"}, {"db": "admin", "role": "restore"}, {"db": "admin", "role": "dbAdminAnyDatabase"}, {"db": "admin", "role": "readWriteAnyDatabase"}]
        }
    );
}
