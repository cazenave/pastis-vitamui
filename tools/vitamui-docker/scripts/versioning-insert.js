use admin;

print("START versioning.js");

db.getCollection("versioning").find({})
var now = new Date();


db.versioning.insert({
    "_id": ObjectId(),
    "version": "1.0.0",
    "date": now,
    "changelog": [
          {
       "script": "/vitamui/scripts/mongo/data/last/00--iam--1.0.0--create_users_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/00--iam--1.0.0--create_users_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/01--iam--1.0.0--01_iam_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/01--iam--1.0.0--01_iam_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/02--iam--1.0.0--02_security_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/02--iam--1.0.0--02_security_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/03--iam--1.0.0--03_application_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/03--iam--1.0.0--03_application_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/04--iam--1.0.0--add_graphic_identity_all_customers_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/04--iam--1.0.0--add_graphic_identity_all_customers_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/05--iam--1.0.0--cas_services_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/05--iam--1.0.0--cas_services_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/06--iam--1.0.0--VITAMUI-2800_init_user_address_ref.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/06--iam--1.0.0--VITAMUI-2800_init_user_address_ref.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/07--iam--1.0.0--101_iam_client1_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/07--iam--1.0.0--101_iam_client1_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/08--iam--1.0.0--101_iam_client2_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/08--iam--1.0.0--101_iam_client2_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/09--iam--1.0.0--101_iam_system_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/09--iam--1.0.0--101_iam_system_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/10--iam--1.0.0--101_iam_system_plus_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/10--iam--1.0.0--101_iam_system_plus_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/11--iam--1.0.0--102_security_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/11--iam--1.0.0--102_security_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/12--iam--1.0.0--105_cas_demo.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/12--iam--1.0.0--105_cas_demo.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/13--iam--1.0.0--security.populate_certificates_dev.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/13--iam--1.0.0--security.populate_certificates_dev.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/14--iam--1.0.0--201_iam_dev.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/14--iam--1.0.0--201_iam_dev.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/15--iam--1.0.0--202_cas_dev.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/15--iam--1.0.0--202_cas_dev.js")
      },
          {
       "script": "/vitamui/scripts/mongo/data/last/16--iam--1.0.0--206_test_data_dev.js",
       "content":  md5sumFile("/vitamui/scripts/mongo/data/last/16--iam--1.0.0--206_test_data_dev.js")
      },
         ]
});


db.versioning.insert({
    "_id": ObjectId(),
    "version": "1.0.1",
    "date": now,
    "changelog": [
         ]
});


print("END versioning.js");
